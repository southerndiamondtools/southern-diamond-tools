Southern Diamond Tools is one of Australia's leading tool suppliers.

We provide a wide range of tools to meet the needs of various trades and professions. In addition to its tool hire division that services the construction industry throughout Australia's East coast with diamond tooling solutions for concrete drilling, core drilling and much more. 

With honesty, integrity, and fairness as long-standing values at the core of our reputation, we will continue to deliver exceptional service to our customers.

Our ultimate goal is to build a long-term business relationship with you.

Website: https://southerndiamondtools.com.au/
